'use strict'
const join = require('path').join
const BrowserWindow = require('electron').BrowserWindow
const config = require('./config')

function createNewWindow() {
  const lastWindowState = config.get('lastWindowState')
  return new BrowserWindow({
    minWidth: 615,
    x: lastWindowState.x,
    y: lastWindowState.y,
    width: lastWindowState.width,
    height: lastWindowState.height,
    icon: join(__dirname, '../build/icon_256.png'),
    title: 'Keep',
    titleBarStyle: 'hiddenInset',
    webPreferences: {
      preload: `${__dirname}/browser.js`
    }
  })
}

const windows = []
function hasOpenWindows() {
  return windows.filter(win => !win.isDestroyed() && win.isVisible()).length > 0
}

module.exports = function createMainWindow(handleResize, handleClosed, handleQuit) {
  const window = createNewWindow();

  window.loadURL('https://keep.google.com', { userAgent: 'Chrome' })
  window.on('resize', handleResize)
  window.on('closed', handleClosed)
  windows.push(window)

  window.on("close", (event) => {
    if(!window.isVisible())
      return;

    event.preventDefault();
    window.hide();

    if (!hasOpenWindows())
      handleQuit()
  });

  const handleNewWindow = (event, url) => {
    event.preventDefault()

    let child = createNewWindow();
    child.loadURL(url);
    child.on("closed", (event) => {
      if (!hasOpenWindows())
        handleQuit()
    });

    child.webContents.on('new-window', handleNewWindow)
    windows.push(child)
  }

  window.webContents.on('new-window', handleNewWindow)
  return window
}
